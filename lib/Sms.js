var request = require('request');
var Utils = require('./Utils')


var j = request.jar();
var headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux i686)',
    'Origin': 'http://www.smsnexus.com.br',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
};

function send(n, text, fn){
    console.log('SENDIND TO...', n)
    Utils.getCarrier(n, function(err, resp){
        resp = JSON.parse(resp)
        resp = resp[0]

        if( resp.current_carrier == 'invalido' ){
            return fn({ number: 'Invalid',
                        info: 
                        { 
                            meta: {region: "", carrier: ""},
                            resultado: false,
                            campanha_id: 59653,
                            timex: +new Date()
                        }
                    } 
                
            );
        }
        if( err  ) return fn( new Error({error: 'Invalid number', code: "PhoneBrazilliamFormatException"}) );
        
        var loginLink = 'http://www.smsnexus.com.br/portal/Login/Index/?ReturnUrl=%2fportal';

        request.get({
            url: loginLink, 
            jar: j, 
            headers: headers
        }, function (err, response, html) {

            console.log('> get cookie...')

            var dataString = '$Login=1\'+or+\'1\'+%3D+\'1&Senha=1\'+or+\'1\'+%3D+\'1&proposta=N';          

            request({
                url: 'http://www.smsnexus.com.br/portal/Login/Index/',
                method: 'POST',
                headers: headers,
                body: dataString,
                jar: j
            }, function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log('> OK')
                    sendSMS(j, n, text, resp, fn);
                }
            });
        });
    })
}


function sendSMS(j, n, text, op, fn){

    console.log('> Logged in sending to %s ...', n);
    var n = ['(',n.slice(0,2), ')', n.slice(2,7), '-', n.slice(7,n.length)].join('');


    var dataString = 'Id=0&Campanha=NNN&Contatos='+n+'&DataAgendamento=&HoraAgendamento=&Mensagem='+text;   

    request({
        url: 'http://www.smsnexus.com.br/portal/SMS/EnvioRapido',
        method: 'POST',
        headers: headers,
        body: dataString,
        jar: j
    }, function(error, response, body) {
        
        if (!error && response.statusCode == 200) {
            console.log('> Sent to %s ...', n);
            var body = JSON.parse(body);

            body.phone = n;
            body.timex = +new Date();
            body.phoneInt = '+55'+n;
            body.meta = op;
            fn(null, body);
        }
    });        
}

module.exports = send;



